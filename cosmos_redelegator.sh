#!/bin/bashcosmos token prefix-list
# Default variables
prefix=""
keyring_backend="file"
create_config="false"

# Options
. <(wget -qO- https://raw.githubusercontent.com/SecorD0/utils/main/colors.sh) --
option_value(){ echo $1 | sed -e 's%^--[^=]*=%%g; s%^-[^=]*=%%g'; }
while test $# -gt 0; do
	case "$1" in
	-h|--help)
		. <(wget -qO- https://raw.githubusercontent.com/SecorD0/utils/main/logo.sh)
		echo
		echo -e "${C_LGn}Functionality${RES}: the script automatically delegates and withdraws tokens from a Cosmos node"
		echo
		echo -e "Usage: script ${C_LGn}[OPTIONS]${RES}"
		echo
		echo -e "${C_LGn}Options${RES}:"
		echo -e "  -h,  --help           show help page"
		echo -e "  -p,  --prefix PREFIX  prefix of token"
		echo -e "                        PREFIX is '' (default), '${C_LGn}u${RES}' (1*10^-6), '${C_LGn}a${RES}' (1*10^-18)"
		echo -e "  -kb TYPE              TYPE of keyring's backend"
		echo -e "                        TYPE is '${C_LGn}os${RES}', '${C_LGn}file${RES}' (default)"
		echo -e "  -cc, --create-config  create or overwrite a config file"
		echo
		echo -e "${C_LGn}How to use${RES}:"
		echo -e " ${C_C}1)${RES} Install Tmux"
		echo -e "${C_LY}sudo apt install tmux -y${RES}"
		echo -e " ${C_C}2)${RES} Open new Tmux window"
		echo -e "${C_LY}tmux new -s delegator${RES}"
		echo -e " ${C_C}3)${RES} Enter the node directory, for example"
		echo -e "${C_LY}cd $HOME/.umee/${RES}"
		echo -e " ${C_C}4)${RES} Create the config file"
		echo -e "${C_LY}. <(wget -qO- https://gitlab.com/SecorD0/Cosmos/-/raw/main/cosmos_redelegator.sh)${RES}"
		echo -e " ${C_C}5)${RES} Open the config file via MobaXterm notepad, Nano, Vi, etc. and customize it"
		echo -e "${C_LY}cat cr_config.sh${RES}"
		echo -e " ${C_C}6)${RES} Run the script with the desired token prefix, for example"
		echo -e "${C_LY}. <(wget -qO- https://gitlab.com/SecorD0/Cosmos/-/raw/main/cosmos_redelegator.sh) -p u${RES}"
		echo -e " ${C_C}7)${RES} Detach the window"
		echo -e "Via hotkey: ${C_LY}Ctrl+B, D${RES}"
		echo -e "Via command: ${C_LY}tmux detach${RES}"
		echo
		echo -e " Command to attach the window"
		echo -e "${C_LY}tmux attach -t delegator${RES}"
		echo
		echo -e "${C_LGn}Useful URLs${RES}:"
		echo -e "https://t.me/letskynode — node Community"
		echo
		return 0 2>/dev/null; exit 0
		;;
	-p*|--prefix*)
		if ! grep -q "=" <<< $1; then shift; fi
		prefix=`option_value $1`
		shift
		;;
	-kb*)
		if ! grep -q "=" <<< $1; then shift; fi
		keyring_backend=`option_value $1`
		shift
		;;
	-cc|--create-config)
		create_config="true"
		shift
		;;
	*|--)
		break
		;;
	esac
done
# Functions
printf_n(){ printf "$1\n" "${@:2}"; }
# Actions
if [ ! -f cr_config.sh ] || [ "$create_config" = "true" ]; then
	sudo tee <<EOF >/dev/null cr_config.sh
#!/bin/bash
# Acceptable use:
# cr_daemon="/usr/bin/umeed" # text
# cr_validator_address="\$umee_wallet_address" # variable
# cr_node_tcp=\`grep -oPm1 "(?<=^laddr = \")([^%]+)(?=\")" $HOME/.umee/config/config.toml\` # command output

cr_daemon="/usr/bin/umeed"
cr_chain_id="umee-betanet-2"
cr_token_name="umee"
cr_node_tcp=\`grep -oPm1 "(?<=^laddr = \")([^%]+)(?=\")" $HOME/.umee/config/config.toml\`

cr_validator_address="umeevaloper1___"
cr_wallet_address="umee1___"
cr_wallet_password="___"
cr_gas="200000"
cr_fees="200"
cr_remaining_balance="100.25"

cr_delay=7200 # how often to restart the script (in secs)
EOF
	printf_n "${C_LGn}Config was created!${RES}"
	return 0 2>/dev/null; exit 0
fi
sudo apt install bc -y &>/dev/null
chmod +x cr_config.sh
while true; do
	. ./cr_config.sh
	if [ ! -n "$cr_fees" ]; then
		cr_fees="200"
	fi
	printf_n "\n${C_LGn}Withdraw rewards from delegation...${RES}"
	echo -e "${cr_wallet_password}\n" | $cr_daemon tx distribution withdraw-all-rewards --chain-id "$cr_chain_id" --from "$cr_wallet_address" --gas "$cr_gas" --fees "${cr_fees}${prefix}${cr_token_name}" --node "$cr_node_tcp" --keyring-backend "$keyring_backend" -y 1>/dev/null
	sleep 20
	cr_balance=`$cr_daemon query bank balances "$cr_wallet_address" -o json --node "$cr_node_tcp" | jq -r ".balances[0].amount"`
	if [ "$prefix" = "u" ]; then
		cr_balance_n=`bc -l <<< "${cr_balance}/1000000"`
	elif [ "$prefix" = "a" ]; then
		cr_balance_n=`bc -l <<< "${cr_balance}/1000000000000000000"`
	else
		cr_balance_n="$cr_balance"
	fi
	if grep -q "$cr_wallet_address" <<< `$cr_daemon debug addr "$cr_validator_address"`; then
		printf_n "Current balance: ${C_LGn}%.7f${RES} ${cr_token_name}\n\n${C_LGn}Withdraw commission rewards...${RES}" "$cr_balance_n"
		echo -e "${cr_wallet_password}\n" | $cr_daemon tx distribution withdraw-rewards "$cr_validator_address" --chain-id "$cr_chain_id" --from "$cr_wallet_address" --commission --gas "$cr_gas" --fees "${cr_fees}${prefix}${cr_token_name}" --node "$cr_node_tcp" --keyring-backend "$keyring_backend" -y 1>/dev/null
		sleep 20
		cr_balance=`$cr_daemon query bank balances "$cr_wallet_address" -o json --node "$cr_node_tcp" | jq -r ".balances[0].amount"`
		if [ "$prefix" = "u" ]; then
			cr_balance_n=`bc -l <<< "${cr_balance}/1000000"`
		elif [ "$prefix" = "a" ]; then
			cr_balance_n=`bc -l <<< "${cr_balance}/1000000000000000000"`
		else
			cr_balance_n="$cr_balance"
		fi
	fi
	printf_n "Current balance: ${C_LGn}%.7f${RES} ${cr_token_name}\n\n${C_LGn}Staking...${RES}" "$cr_balance_n"
	if [ `bc <<< "$cr_balance_n>$cr_remaining_balance"` -eq "1" ]; then
		if [ "$prefix" = "u" ]; then
			cr_delegate_balance=`bc -l <<< "${cr_balance}-${cr_remaining_balance}*1000000"`
			cr_delegate_balance_n=`bc -l <<< "${cr_delegate_balance}/1000000"`
		elif [ "$prefix" = "a" ]; then
			cr_delegate_balance=`bc -l <<< "${cr_balance}-${cr_remaining_balance}*1000000000000000000"`
			cr_delegate_balance_n=`bc -l <<< "${cr_delegate_balance}/1000000000000000000"`
		else
			cr_delegate_balance=`bc -l <<< "${cr_balance}-${cr_remaining_balance}"`
			cr_delegate_balance_n="$cr_delegate_balance"
		fi
		printf_n "Stake ${C_LGn}%.7f${RES} ${cr_token_name}" "$cr_delegate_balance_n"
		echo -e "${cr_wallet_password}\n" | $cr_daemon tx staking delegate "$cr_validator_address" "${cr_delegate_balance}${prefix}${cr_token_name}" --chain-id "$cr_chain_id" --from "$cr_wallet_address" --gas "$cr_gas" --fees "${cr_fees}${prefix}${cr_token_name}" --node "$cr_node_tcp" --keyring-backend "$keyring_backend" -y 1>/dev/null
	else
		printf_n "Don't stake because ${C_LGn}%.7f<%.7f${RES}" "$cr_balance_n" "$cr_remaining_balance"
	fi
	unset cr_wallet_password
	printf_n "\nSleep for ${C_LGn}${cr_delay}${RES} secs"
	sleep $cr_delay
done
